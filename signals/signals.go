package signals

import (
    "os"
    "os/signal"
    "sync"
    "fmt"
    "syscall"
  . "gitlab.com/mrvik/goautolock/types"
)

func signalLoop(config *Config, state *RuntimeInformation, wg *sync.WaitGroup){
    wg.Add(1);
    defer wg.Done();
    sigs:=make(chan os.Signal, 1);
    signal.Notify(sigs, syscall.SIGUSR1, syscall.SIGUSR2, syscall.SIGINT, syscall.SIGTERM);
    for ;!state.CheckExitFlag();{
        sig:=<-sigs;
        switch {
            case sig == syscall.SIGUSR1:{
                if ! state.ReadLocked() {
                    state.Locknow<-true;
                } else {
                    fmt.Fprintf(os.Stderr, "Signal caught but screen is already locked\n");
                }
            }
            case sig == syscall.SIGUSR2:{
                state.IncrementCaffeine(config.Caffeine);
                fmt.Printf("Setting caffeine to %d\n", state.ReadCaffeine());
            }
            case sig==syscall.SIGTERM||sig==syscall.SIGINT:{
                fmt.Printf("Exiting due to %q signal\n", sig);
                close(state.Exit);
            }
            default:{
                fmt.Fprintf(os.Stderr, "Uncaught signal %q", sig);
            }
        }
    }
}

func CatchSignals(config *Config, info *RuntimeInformation, wg *sync.WaitGroup){
    signalLoop(config, info, wg);
}
