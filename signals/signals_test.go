package signals

import (
  . "gitlab.com/mrvik/goautolock/types"
    "testing"
    "syscall"
    "os"
    "time"
    "sync"
)

func TestCatchSignals(t *testing.T){
    var times uint64;
    pid:=os.Getpid();
    config:=Config{Caffeine: 10, LockInterval: 20};
    ri:=CreateRuntimeInfo();
    wg:=new(sync.WaitGroup);
    wg.Add(2);
    go CatchSignals(&config, &ri, wg);
    times=2;
    time.Sleep(200*time.Millisecond);
    killTimes(pid,syscall.SIGUSR2, int(times));
    if ri.ReadCaffeine() != config.Caffeine*times {
        t.Errorf("Caffeine failed. Expected %d but got %d\n",config.Caffeine*times, ri.Caffeine);
    } else {
        t.Logf("Caffeine signal OK\n");
    }
    killTimes(pid, syscall.SIGUSR1, 1);
    if !checkBoolFromChannel(ri.Locknow) {
        t.Error("Inmediate lock failed. Didn't catch signal\n");
    } else {
        t.Logf("Inmediate lock OK");
    }
    // killTimes(pid, syscall.SIGTERM, 1);
    // if !ri.Exit {
        // t.Error("Exit failed. Didn't catch signal\n");
    // }
}

func killTimes(pid int, signal syscall.Signal, times int){
    for i:=0; i<times; i++ {
        syscall.Kill(pid, signal);
        time.Sleep(100*time.Millisecond);
    }
    time.Sleep(2000*time.Millisecond);
}

func checkBoolFromChannel(channel chan bool) bool{
    select{
    case v:=<-channel:
        return v;
    default:
        return false;
    }
}

