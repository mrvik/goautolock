package exec

import (
    "testing"
  . "gitlab.com/mrvik/goautolock/types"
)

func TestExecuteLocker(t *testing.T){
    configs:=[]Config{
        {
            Locker: "notexistentcommand", //Should not exist
            LockerArgs: []string{"0"},
        },
        {
            Locker: "/usr/bin/false",
            LockerArgs: []string{"0"},
        },
        {
            Locker: "/usr/bin/true",
            LockerArgs: []string{"0"},
        },
    }
    mustFail:=[]bool{true, true, false};
    for k,v:=range configs {
        t.Logf("Testing config %v\n",v);
        ch:=make(chan error, 1);
        testAndRecover(&v, ch);
        err:=<-ch;
        m:=mustFail[k];
        if err == nil && m{
            t.Errorf("Test must fail but didn't");
        } else if err!=nil && !m{
            t.Errorf("Test shouldn't fail, but did. %q",err);
        }
    }
}

func testAndRecover(config *Config, res chan error){
    defer func(){
        if r:=recover(); r!=nil{
            if err,ok:=r.(error); ok {
                res<-err;
                return;
            }
        }
        res<-nil;
    }();
    ExecuteLocker(config);
}

func should(f bool)string{
    if f {
        return "should"
    }else{
        return "shouldn't"
    }
}

