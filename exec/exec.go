package exec

import (
    "os"
    "os/exec"
  . "gitlab.com/mrvik/goautolock/types"
)

func ExecuteLocker(config *Config){
    cmd:=exec.Cmd{
        Path: config.Locker,
        Args: config.LockerArgs,
        Stderr: os.Stderr,
    }
    err:=cmd.Run();
    if err != nil {
        panic(err);
    }
}
