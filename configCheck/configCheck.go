package configCheck

import (
    "fmt"
    "os"
    "os/exec"
  . "gitlab.com/mrvik/goautolock/types"
)

func Check(config *Config, runtimeInfo *RuntimeInformation) bool{
    defer func() bool{
        if r:=recover(); r!=nil {
            fmt.Fprintln(os.Stderr, r);
            return false;
        }
        return true;
    }();
    runtimeInfo.ConnectDisplay(config.Display);
    correctLocker:=checkPath(config.Locker);
    config.Locker=correctLocker;
    return true;
}


func checkPath(name string) string{
    path,err:=exec.LookPath(name);
    if err != nil{
        panic(err);
    }
    return path;
}
