package configCheck

import (
  . "gitlab.com/mrvik/goautolock/types"
    "os"
    "regexp"
    "testing"
)

func TestCheck(t *testing.T){
    //Test an incorrect locker
    runtime:=RuntimeInformation{};
    display:=os.Getenv("DISPLAY");
    configs:=[]Config{
        {
            Locker: "/this/path/doesnt exist",
            Display: ":-1", //An invalid display
        },
        {
            Locker: "/bin/sh", //This one is correct
            Display: ":-1", //Incorrect
        },
        {
            Locker: "/bin/sh", //Correct
            Display: display, //There should be a XServer started
        },
        {
            Locker: "sh", //Should change to /bin/sh or /usr/bin/sh
            Display: display,
        },
    }
    expected:=[]bool{false, false, true, true};
    expectedLockers:=[]string{"","/bin/sh", "/bin/sh", "(.*\\/.{0,1}bin\\/sh)"}

    for k,v := range configs {
        t.Logf("Testing %v\n",v);
        result:=Check(&v, &runtime);
        if result != expected[k] {
            t.Errorf("Result of config %v was %v, but %v was expected\n",v,result,expected[k]);
        }
        if expectedLocker:=expectedLockers[k]; expectedLocker!="" {
            if res,err:=regexp.MatchString(expectedLocker, v.Locker); !res||err!=nil{
                if err != nil {
                    t.Error(err);
                }
                t.Errorf("Bad locker. %s expected, but %s was set\n",expectedLocker,v.Locker);
            }
        }
    }
}
