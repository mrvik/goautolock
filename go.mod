module gitlab.com/mrvik/goautolock

go 1.12

require (
	github.com/akamensky/argparse v1.2.0
	github.com/mattn/go-gtk v0.0.0-20191030024613-af2e013261f5 // indirect
	github.com/mqu/go-notify v0.0.0-20130719194048-ef6f6f49d093
)
