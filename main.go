package main

import (
    "fmt"
    "os"
    "io/ioutil"
    "sync"
    "github.com/akamensky/argparse"
    "gitlab.com/mrvik/goautolock/configCheck"
  . "gitlab.com/mrvik/goautolock/types"
    "gitlab.com/mrvik/goautolock/signals"
    "gitlab.com/mrvik/goautolock/watcher"
)

/*
 * Caffeine adds time to RuntimeInformation. Next time caffeine is gone and must lock at estipulated time
*/

var (
    wg sync.WaitGroup;
)

func main(){
    config:=parseArguments();
    if config == nil {
        return;
    }
    if filename:=genPidfile(); detectRunning(filename) {
        return;
    }else{
        defer removeFile(filename);
    }
    info:=CreateRuntimeInfo();
    defer exit(config, &info);
    chk:=configCheck.Check(config, &info);
    if !chk {
        return;
    }
    if !config.DisallowKill {
        go signals.CatchSignals(config, &info, &wg);
    }
    wg.Add(2);
    go watcher.TimeIdleWatcher(config, &info, &wg);
    wg.Wait();
}

func parseArguments() *Config{
    parser := argparse.NewParser("goautolock", "Killable autolocker written in GO");
    defer recoverParseArguments();
    initialConfig:=InitialConfig{
        Locker: parser.String("l", "locker", &argparse.Options{Required: true, Help: "Locker to be used"}),
        LockerArgs: parser.String("a", "args", &argparse.Options{Required: false, Help: "Arguments for locker"}),
        LockInterval: parser.String("t", "time", &argparse.Options{Required: false, Help: "Timeout interval (in seconds)", Default: "60"}),
        NotificationInterval: parser.String("n", "notify", &argparse.Options{Required: false, Help: "Notify on timeout (in seconds)", Default: "0"}),
        Caffeine: parser.String("c", "caffeine", &argparse.Options{Required: false, Help: "Time added per caffeine signal", Default: "10"}),
        DisallowKill: parser.Flag("", "disallow-kill", &argparse.Options{Required: false, Help: "Don't catch signals to lock or extend lock"}),
    }
    err := parser.Parse(os.Args);
    if err != nil {
        panic(err);
    }

    config:=Config{};
    config.FromInitial(&initialConfig);
    config.Display=os.Getenv("DISPLAY");
    if config.Display=="" {
        panic("DISPLAY variable not set. Cannot connect to display");
    }
    print(config.ToString());
    return &config;
}

func genPidfile() string{
    xdgRD:=getEnvFallback("XDG_RUNTIME_DIR", "/tmp");
    xdgSID:=getEnvFallback("XDG_SESSION_ID", "0"); //Session ID must exist
    return fmt.Sprintf("%s/goautolock-%s.pid", xdgRD, xdgSID);
}

func detectRunning(filename string) bool{
    content,err:=ioutil.ReadFile(filename);
    if err!=nil{
        if os.IsNotExist(err){
            writeRunning(filename);
            return false;
        }
        fmt.Fprintln(os.Stderr, err);
    }
    fmt.Fprintf(os.Stderr, "Goautolock already running. PID %s, PID File on %q\n", string(content), filename);
    return true;
}

func writeRunning(filename string){
    content:=fmt.Sprintf("%d", os.Getpid());
    err:=ioutil.WriteFile(filename, []byte(content), 0600);
    if err != nil{
        panic(err);
    }
}

func removeFile(filename string){
    err:=os.Remove(filename);
    if err!=nil{
        panic(err);
    }
}

func recoverParseArguments(){
    if r := recover(); r != nil {
        fmt.Println("Error parsing arguments: ", r);
    }
}

func getEnvFallback(name, fallback string) string{
    val:=os.Getenv(name);
    if val==""{
        return fallback;
    }
    return val;
}

func exit(config *Config, info *RuntimeInformation){
    //Close connection to X11
    if info.DisplayPtr != nil {
        info.DisconnectDisplay();
    }
}

