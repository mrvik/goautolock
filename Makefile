.PHONY: default all build dev-build go-install uninstall clean test citest

PREFIX := /usr/local
DESTDIR :=

FLAGS:=-ldflags='-s -w -extldflags "-z noexecstack -z relro -z now"'
NODEBUGFLAGS:=-gcflags=all=-trimpath=${PWD} -asmflags=all=-trimpath=${PWD}
DEBUGFLAGS:=-ldflags='--compressdwarf=false -extldflags=-zrelro -extldflags=-znow'
DEVFLAGS :=-race
PRODFLAGS := -buildmode=pie
PKGNAME := goautolock

default: build

all: | clean build

build:
	go build -v ${PRODFLAGS} ${FLAGS} ${NODEBUGFLAGS} -o ${PKGNAME}

build-with-debug:
	go build -v ${PRODFLAGS} ${DEBUGFLAGS} -o ${PKGNAME}

dev-build:
	go build -v ${DEVFLAGS} ${FLAGS} -o ${PKGNAME}-dev

go-install:
	go install -v ${FLAGS} .

install:
	install -Dm755 ${PKGNAME} ${DESTDIR}${PREFIX}/bin/${PKGNAME}
	install -Dm644 doc/${PKGNAME}.1.groff ${DESTDIR}${PREFIX}/share/man/man1/${PKGNAME}.1

uninstall:
	rm -f ${DESTDIR}/${PREFIX}/bin/${PKGNAME}
	rm -f ${DESTDIR}/${PREFIX}/share/man/man1/${PKGNAME}.1

clean:
	rm -f ${PKGNAME}
	rm -f ${PKGNAME}-dev

test:
	go vet ${DEVFLAGS} ${FLAGS} $(shell go list ./...)
	go test ${DEVFLAGS} ${FLAGS} $(shell go list ./...)
	go test ${PRODFLAGS} ${FLAGS} $(shell go list ./...)

citest:
	bash container/runXand.sh test
