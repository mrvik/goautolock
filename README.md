[![Copr build status](https://copr.fedorainfracloud.org/coprs/mrvik/goautolock/package/goautolock/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/mrvik/goautolock/package/goautolock/)

# GoAutolock
Lock your X session automatically

`goautolock(1)` tries to imitate some `xautolock(1)` features and add some others like the following:
- Catch `SIGUSR1` and `SIGUSR2`
  - `SIGUSR1` -> Call locker now
  - `SIGUSR2` -> Add caffeine (delay some extra seconds the lock screen)
  - Additionally use `SIGSTOP` and `SIGCONT` to pause or continue locking automatically. (Please note that `SIGCONT` won't fire a lock because `goautolock(1)` is not the one that watches idle time)

If you want any other feature, add a feature request

## Dependencies

- Makedepends
  - `make`
  - `go`
  - `glib2-devel`
  - Some go modules (downloaded on go build)
  - Headers from the runtime dependencies (-devel or -dev packages on some distributions)

- Depends (runtime)
  - `libxss`
  - `libx11`
  - `libnotify`

