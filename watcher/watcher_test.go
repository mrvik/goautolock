package watcher

import (
  . "gitlab.com/mrvik/goautolock/types"
    "testing"
    "os"
)

func TestCheckTime(t *testing.T){
    state:=CreateRuntimeInfo();
    state.ConnectDisplay(os.Getenv("DISPLAY"));
    st:=idleTimeStruct{treshold: 1,state:&state};
    ch:=make(chan bool, 1);
    time:=st.getTime();
    st.checkTime(ch);
    locked:=false;
    t.Logf("Time idle: %d, Treshold: %d\n", time, st.treshold);
    select{
    case rt,ok:=<-ch:
        if !ok {
            t.Errorf("Error reading from lock channel");
        }else{
            locked=rt;
        }
    default:
        locked=false;
    }

    if time>=st.treshold && !locked {
        t.Errorf("Time idle (%d) over treshold (%d) and not locked\n", time, st.treshold);
    } else if time<st.treshold && locked {
        t.Errorf("Time idle (%d) lower than treshold (%d) but locked\n", time, st.treshold);
    }
    t.Logf("Time checker OK. Locked: %v\n", locked);
}

