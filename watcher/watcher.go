package watcher

import (
    "fmt"
    "time"
    "sync"
  . "gitlab.com/mrvik/goautolock/types"
    "gitlab.com/mrvik/goautolock/exec"
    "github.com/mqu/go-notify"
)

const (
    NOTIFICATION_TITLE="Locking soon"
    NOTIFICATION_FORMAT="Locking screen on %d seconds"
    NOTIFICATION_TIMEOUT=5000
    NOTIFICATION_ICON="dialog-information"
)

type idleTimeStruct struct{
    useNotify, notified bool;
    unlocked chan bool;
    treshold, ntreshold uint64;
    state *RuntimeInformation;
}

func (st *idleTimeStruct) getTime() uint64{
    return st.state.GetIdleTime()/1000; //scrnsaver returns milliseconds, we want seconds
}

func (st *idleTimeStruct) checkTime(ready chan bool) bool{ //returns true if lock triggered, false otherwise
    time:=st.getTime();
    cf:=st.state.ReadCaffeine();
    if tr:=st.treshold+cf; time>=tr {
        ready<-true;
        return true;
    } else if ntr:=st.ntreshold+cf; st.useNotify&&!st.notified&&time>=ntr {
        showNotification(NOTIFICATION_TITLE, fmt.Sprintf(NOTIFICATION_FORMAT, tr-time), NOTIFICATION_TIMEOUT);
        st.notified=true;
    } else if time<ntr{
        st.notified=false;
    }
    return false;
}

func (st *idleTimeStruct) afterLock(){
    st.unlocked<-true;
    st.state.WriteCaffeine(0);
    st.state.WriteLocked(false);
}

func TimeIdleWatcher(config *Config, state *RuntimeInformation, wg *sync.WaitGroup){
    lockIdle:=idleTimeStruct{treshold: config.LockInterval, ntreshold: config.NotificationInterval,state: state, unlocked: make(chan bool,1)};
    lockIdle.useNotify=config.NotificationInterval>0;
    if lockIdle.useNotify {
        notify.Init("goautolock");
    }
    go func(){
        defer wg.Done();
        for ;!state.CheckExitFlag();{
            if state.ReadLocked() || lockIdle.checkTime(state.Locknow) {
                _=<-lockIdle.unlocked;
            } else {
                time.Sleep(1 * time.Second);
            }
        }
    }();
    defer wg.Done();
    waitForLock(state.Locknow, &lockIdle, config);
}

func waitForLock(ready chan bool, st *idleTimeStruct, config *Config){
    exitFlag:=false;
    for ;!exitFlag; {
        select{
            case v,ok:=<-st.state.Locknow:{
                if v&&ok {
                    fmt.Println("Ready to lock screen!");
                    st.state.WriteLocked(true);
                    exec.ExecuteLocker(config);
                    st.afterLock();
                }
            }
            case v,ok:=<-st.state.Exit:{
                exitFlag=v||!ok; //Exit if exit channel is closed
            }
        }
    }
}

func showNotification(title, description string, timeout int32){
    nt:=notify.NotificationNew(title, description, NOTIFICATION_ICON);
    nt.SetTimeout(timeout);
    nt.Show();
}

