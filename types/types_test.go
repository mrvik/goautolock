package types

import (
    "testing"
    "errors"
    "fmt"
    "reflect"
    "math/rand"
)

func TestFromInitial(t *testing.T){
    locker:="i3lock";
    lockInterval:="32984723847";
    lockInterval2:="-2";
    notificationInterval:="0000023234234234234234";
    caffeine:="0x00002";
    caffeine2:="0";
    disallowKill:=false;
    initialConfigs:=[]InitialConfig{
        {
            Locker: &locker, // Skipping locker arguments (to be tested later)
            LockInterval: &lockInterval,
            NotificationInterval: &notificationInterval,
            Caffeine: &caffeine,
            DisallowKill: &disallowKill,
        },
        {
            Locker: &locker,
            LockInterval: &lockInterval2,
        },
        {
            Locker: &locker,
            LockInterval: &lockInterval,
            NotificationInterval: &notificationInterval,
            Caffeine: &caffeine2,
            DisallowKill: &disallowKill,
        },
    }

    expectedConfigs:=[]Config{
        {}, // Panic
        {}, // Panic
        {
            Locker: locker,
            LockInterval: 32984723847,
            NotificationInterval: 23234234234234234,
            Caffeine: 0,
        },
    }

    shouldPanic:=[]bool{true,true,false};

    for k,v := range initialConfigs {
        expected:=expectedConfigs[k];
        final:=Config{};
        var err error;
        func(){
            defer func(){
                if r:=recover(); r!= nil {
                    err=getErrorFromRecover(r);
                }
            }();
            final.FromInitial(&v);
        }();
        if err!=nil && shouldPanic[k]{
            t.Log("Panicked as expected");
            continue;
        } else if err != nil{
            t.Errorf("Unexpected error when parsing arguments: %s\n", err);
        }
        st1:=fmt.Sprintf("%v", final);
        st2:=fmt.Sprintf("%v", expected);
        if st1 == st2 {
            t.Logf("Returned %s equals %s\n", st1, st2);
        } else {
            t.Errorf("Returned %q doesn't equal %q\n", st1, st2);
        }
    }
}

func TestParseArguments(t *testing.T){
    tests:=[]string{
        "",
        "-a -b -c",
        "-a\\ -b",
        "--arg=value --arg2=value\\ with\\ spaces",
    }
    expectations:=[][]string{};
    expectations=append(expectations, []string{""});
    expectations=append(expectations, []string{"-a", "-b", "-c"});
    expectations=append(expectations, []string{"-a\\ -b"});
    expectations=append(expectations, []string{"--arg=value", "--arg2=value\\ with\\ spaces"});
    for k,v:=range tests {
        expected:=expectations[k];
        final:=parseArguments(&v);
        if !reflect.DeepEqual(expected, final) {
            t.Errorf("Expected %v(%d) but received %v(%d)\n", expected, len(expected), final, len(final));
        } else {
            t.Logf("%v(%d) and %v(%d) are equals\n", expected, len(expected), final, len(final));
        }
    }
}

func TestCreateRuntimeInfo(t *testing.T){
    ri:=CreateRuntimeInfo();
    select{
    case ri.Locknow<-true:
        t.Log("Sent value to Locknow channel");
    default:
        t.Errorf("Couldn't send a value to Locknow channel");
    }

    select{
    case ri.Exit<-true:
        t.Log("Sent value to Exit channel");
    default:
        t.Errorf("Couldn't send a value to Exit channel");
    }

    select{
        case v,ok:=<-ri.Locknow:{
            if v&&ok {
                t.Log("Locknow channel received value");
            }else{
                t.Errorf("Locknow channel received value %v with status %v\n",v,ok);
            }
        }
    default:
        t.Errorf("Locknow channel didn't receive any value");
    }

    select{
        case v,ok:=<-ri.Exit:{
            if v&&ok {
                t.Log("Exit channel received a value");
            } else {
                t.Errorf("Exit channel received a value %v with status %v", v, ok);
            }
        }
    default:
        t.Errorf("Exit channel didn't receive any value");
    }
}

func TestCheckExitFlag(t *testing.T){
    ri:=CreateRuntimeInfo();
    select{
    case ri.Exit<-true:
        t.Logf("Sent message via Exit channel");
    default:
        t.Errorf("Send message via Exit channel failed");
    }
    if r:=ri.CheckExitFlag(); r {
        t.Logf("Message received successfully");
    }else{
        t.Errorf("Message not received here, got %v instead\n", r);
    }
    for i:=0; i<10; i++ {
        if r:=ri.CheckExitFlag(); r {
            t.Logf("Received message %d times\n", i)
        }else{
            t.Errorf("Received %v, expected true\n", r);
        }
    }
}

func BenchmarkCreateRuntimeInfo(b *testing.B){
    for i:=0; i<b.N; i++ {
        CreateRuntimeInfo();
    }
}

func BenchmarkReadLocked(b *testing.B){
    ri:=CreateRuntimeInfo();
    for i:=0; i<b.N; i++ {
        r:=ri.ReadLocked();
        if r != false {
            b.Errorf("ReadLocked should return false (zero value) but returned %v instead\n", r);
        }
    }
}

func BenchmarkReadCaffeine(b *testing.B){
    ri:=CreateRuntimeInfo();
    for i:=0; i<b.N; i++ {
        r:=ri.ReadCaffeine();
        if r != 0 {
            b.Errorf("ReadCaffeine should return 0 (zero value) but returned %d instead\n", r);
        }
    }
}

func BenchmarkWriteLocked(b *testing.B){
    ri:=CreateRuntimeInfo();
    var bl bool;
    for i:=0; i<b.N; i++ {
        ri.WriteLocked(bl);
        bl=!bl;
    }
}

func BenchmarkWriteCaffeine(b *testing.B){
    b.StopTimer();
    m:=createUintMatrix(b.N, rand.Uint64);
    b.StartTimer();
    ri:=CreateRuntimeInfo();
    for i:=0; i<b.N; i++ {
        ri.WriteCaffeine(m[i]);
    }
}

func BenchmarkIncrementCaffeine(b *testing.B){
    b.StopTimer();
    m:=createUintMatrix(b.N, rand.Uint64);
    b.StartTimer();
    ri:=CreateRuntimeInfo();
    for i:=0; i<b.N; i++ {
        ri.IncrementCaffeine(m[i]);
    }
}

func createUintMatrix(times int, fn func()uint64)[]uint64{
    rt:=make([]uint64, times);
    for i:=0; i<times; i++ {
        rt[i]=fn();
    }
    return rt;
}

func getErrorFromRecover(rec interface{}) error{
    switch x:=rec.(type) {
        case string:{
            return errors.New(x);
        }
        case error:{
            return x;
        }
    }
    return errors.New("unkown panic");
}

