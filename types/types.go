package types

import(
    "errors"
    "fmt"
    "sync"
    "strconv"
    "strings"
    "unsafe"
)

// #cgo pkg-config: x11 xscrnsaver
// #include <stdlib.h>
// #include <X11/Xlib.h>
// #include <X11/extensions/scrnsaver.h>
import "C"

type Config struct{
    Locker, Display string;
    LockerArgs []string;
    LockInterval, NotificationInterval, Caffeine uint64;
    DisallowKill bool;
}

type InitialConfig struct{
    Locker, LockerArgs, LockInterval, NotificationInterval, Caffeine *string
    DisallowKill *bool;
}

func (cnf *Config) ToString() string{
    return fmt.Sprintf("Display: %s\nLocker program: %s\nLockerArgs: %v\nLockInterval: %d\nNotificationInterval: %d\nCaffeine: %d\nKill Disallowed: %s\n", cnf.Display, cnf.Locker, cnf.LockerArgs, cnf.LockInterval, cnf.NotificationInterval, cnf.Caffeine, getTF(cnf.DisallowKill));
}

func (cnf *Config) FromInitial(initial *InitialConfig){
    cnf.Locker=*initial.Locker;
    if initial.LockerArgs != nil {
        cnf.LockerArgs=parseArguments(initial.LockerArgs);
    } else {
        cnf.LockerArgs=[]string{};
    }
    cnf.LockInterval=parseIntPtr(initial.LockInterval);
    cnf.NotificationInterval=parseIntPtr(initial.NotificationInterval);
    cnf.Caffeine=parseIntPtr(initial.Caffeine);
    cnf.DisallowKill=*initial.DisallowKill;
}

func parseIntPtr(p *string) uint64{
    res,err := strconv.ParseInt(*p, 10, 64);
    if err != nil {
        panic(fmt.Sprintf("%q Is not an integer", *p));
    }
    if res<0{
        panic(fmt.Sprintf("%d is a negative number",res));
    }
    return uint64(res);
}

func getTF(b bool) string{
    if b {
        return "yes";
    }
    return "no";
}

func parseArguments(args *string)[]string{
    split:=strings.Split(*args, " ");
    return wellSplittedArguments(&split);
}

func wellSplittedArguments(args *[]string) []string{
    var final []string;
    finalct:=0;
    for i:=0; i<len(*args); i++{
        var p *string;
        if i>=1 {
            p=&(*args)[i-1];
        }
        v:=(*args)[i];
        if p!=nil&&strings.HasSuffix(*p, "\\") {
            final[finalct-1]=final[finalct-1]+" "+v;
        } else {
            final=append(final, v);
            finalct++;
        }
    }
    return final;
}

func CreateRuntimeInfo() RuntimeInformation{
    r:=RuntimeInformation{};
    r.Locknow=make(chan bool, 1);
    r.Exit=make(chan bool, 1);
    r.LockedMutex=&sync.Mutex{};
    r.CaffeineMutex=&sync.Mutex{};
    return r;
}

type RuntimeInformation struct{
    Caffeine uint64;
    Locknow, Exit chan bool;
    Locked bool;
    LockedMutex,CaffeineMutex *sync.Mutex;
    DisplayPtr *C.Display;
}

/**
 * This function must return true when goautolock must exit. False otherwise
 */
func (st *RuntimeInformation) CheckExitFlag() bool{
    select{
    case v,ok:=<-st.Exit:
        if ok {
            st.Exit<-v; //Resend exit status
        }
        return v||!ok;
    default:
        return false;
    }
}

func (info *RuntimeInformation) GetIdleTime() uint64{
    var xinfo *C.XScreenSaverInfo;
    xinfo = C.XScreenSaverAllocInfo();
    defer C.free(unsafe.Pointer(xinfo));
    defaultRootWindow:=C.XDefaultRootWindow(info.DisplayPtr);
    C.XScreenSaverQueryInfo(info.DisplayPtr, C.Drawable(defaultRootWindow), xinfo);
    return uint64(xinfo.idle);
}

func (info *RuntimeInformation) ConnectDisplay(name string){
    cstring:=C.CString(name);
    defer C.free(unsafe.Pointer(cstring));
    info.DisplayPtr=C.XOpenDisplay(cstring);
    if info.DisplayPtr == nil{
        panic(errors.New("Can't connect to Display!"));
    }
}

func (info *RuntimeInformation) DisconnectDisplay(){
    C.XCloseDisplay(info.DisplayPtr);
    info.DisplayPtr=nil;
}

func (st *RuntimeInformation) ReadLocked()bool{
    st.LockedMutex.Lock();
    defer st.LockedMutex.Unlock();
    return st.Locked;
}

func (st *RuntimeInformation) ReadCaffeine()uint64{
    st.CaffeineMutex.Lock();
    defer st.CaffeineMutex.Unlock();
    return st.Caffeine;
}

func (st *RuntimeInformation) WriteLocked(locked bool){
    st.LockedMutex.Lock();
    defer st.LockedMutex.Unlock();
    st.Locked=locked;
}

func (st *RuntimeInformation) WriteCaffeine(num uint64){
    st.CaffeineMutex.Lock();
    defer st.CaffeineMutex.Unlock();
    st.Caffeine=num;
}

func (st *RuntimeInformation) IncrementCaffeine(by uint64){
    st.CaffeineMutex.Lock();
    st.Caffeine+=by;
    st.CaffeineMutex.Unlock();
}
