#!/bin/bash

if [ -z "$1" ]; then
        echo "What am I supposed to do?"
        exit 1
fi

startx &>/dev/null&
sleep 5

export DISPLAY=":$(ls /tmp/.X11-unix |head -n1|sed 's/X//g')"

if [ -z "$DISPLAY" ]; then
        echo "Cannot get a display!"
        exit 2
fi
echo "Using $DISPLAY"

make $1 && (killall xterm ; exit 0) || (killall xterm , exit 1)

