package main

import (
    "os"
    "fmt"
    "testing"
    "io/ioutil"
)

func TestGenPidfile(t *testing.T){
    expected:=fmt.Sprintf("%s/goautolock-%s.pid", getEnvFallback("XDG_RUNTIME_DIR", "/tmp"), getEnvFallback("XDG_SESSION_ID", "0"));
    got:=genPidfile();
    if expected!=got{
        t.Errorf("Expecting %q as pidfile but got %q\n", expected, got);
    }
}

func TestGetEnvFallback(t *testing.T){
    tests:=map[string][2]string{
        //varName: {expectedValue, defaultValue}
        "NONEXISTENT_VARIABLE":[2]string{"defaultValue", "defaultValue"},
        "NONEXISTENT_VARIABLE_2":[2]string{"", ""},
        "SHELL":[2]string{os.Getenv("SHELL"), ""},
    };
    for name,par:=range(tests){
        rt:=getEnvFallback(name, par[1]);
        if rt!=par[0] {
            t.Errorf("Expecting %q as value of %s but got %q\n", rt[0], name, rt[1]);
        }
    }
}

func TestWriteRunning(t *testing.T){
    actualPID:=fmt.Sprintf("%d",os.Getpid());
    file:=genPidfile();
    writeRunning(file);
    content,err:=ioutil.ReadFile(file);
    if err!=nil{
        t.Error(err);
        return;
    }
    stringContent:=string(content);
    if stringContent!=actualPID{
        t.Errorf("Expecting %q as PID but file has %q\n", actualPID, stringContent);
    }
}

func TestDetectRunning(t *testing.T){
    rn:=detectRunning(genPidfile());
    if !rn {
        t.Errorf("PIDFile exists but detectRunning asserts not running");
    }
}

func TestRemoveFile(t *testing.T){
    file:=genPidfile();
    removeFile(file);
    _,err:=os.Open(file);
    if !os.IsNotExist(err){
        t.Error(err);
    }
}
